// SAL: This is a copy of Label.qml from KDE/4.9. We need it because we rely on
// properties like "truncated" or "maximumLineCount" which were introduced in
// QtQuick 1.1, but Label.qml form KDE/4.8 only imports QtQuick 1.0.
/*
*   Copyright (C) 2011 by Marco Martin <mart@kde.org>
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU Library General Public License as
*   published by the Free Software Foundation; either version 2, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Library General Public License for more details
*
*   You should have received a copy of the GNU Library General Public
*   License along with this program; if not, write to the
*   Free Software Foundation, Inc.,
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/**Documented API
Inherits:
        Text

Imports:
        QtQuick 1.1
        org.kde.plasma.core

Description:
    This is a label which uses the plasma theme.
    The characteristics of the text will be automatically set
    according to the plasma theme. If you need a more customized
    text item use the Text component from QtQuick.

Properties:
    string text:
    The most important property is "text".
    For the other ones see the primitive QML Text element

Methods:
    See the primitive QML Text element

Signals:
    See the primitive QML Text element
**/

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore

Text {
    id: root

    height: Math.max(paintedHeight, theme.defaultFont.mSize.height*1.6)
    verticalAlignment: lineCount > 1 ? Text.AlignTop : Text.AlignVCenter

    font.capitalization: theme.defaultFont.capitalization
    font.family: theme.defaultFont.family
    font.italic: theme.defaultFont.italic
    font.letterSpacing: theme.defaultFont.letterSpacing
    font.pointSize: theme.defaultFont.pointSize
    font.strikeout: theme.defaultFont.strikeout
    font.underline: theme.defaultFont.underline
    font.weight: theme.defaultFont.weight
    font.wordSpacing: theme.defaultFont.wordSpacing
    color: theme.textColor
}
